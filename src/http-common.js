import axios from "axios";

export default axios.create({
    baseURL: 'http://127.0.0.1:8000/api',
    headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json',
        'Authorization': "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiY2UxYWI2ZGY5ZTAxMGNlMjEwOWRmYWVkMTEwMTY4MWE1NWRlZDRkNGQyMTZkNDI4YzY4YWZiZjI1YzcxZmM5NTk0MTUxOGNhMmI3MmM5MTIiLCJpYXQiOjE2NjkzOTQ3NTEuNTc2ODIzLCJuYmYiOjE2NjkzOTQ3NTEuNTc2ODI5LCJleHAiOjE3MDA5MzA3NTEuNTU3NTA1LCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.YniEW61e9IbX5qU6rO5IywyfWqyrperhuJgkFJKqROTxu0BYWYolmFkoGYpBLXUlB9xiBHrOmf1CO5fAFAgoOtIqGtkIB_L_w4y1FF4j8qMjUd98y9jidISp_YSg8kNh4URPFnC-D_szE6kxM8DkeuyibIkrKesBspW7RXqhXq6SSHYFMIIFBjwzpZYNin5bllQ-9zuNNzpRMKUg5JnZ4aCyYqggeQ0RlLNccpB6h6pX93_zW0-164pEMbbyFBIpvC0yMeSDksjC7zalJGCLJQdIqS5WS3izVP6eXDvDuYGJcIb8yGwvH-uTJdZjh3dQKvboY5KQWRTHrqJ6UEbmjwlJwdld7akN4P6TKjZVIaAe8dtRXG4QXaH5-fLmAcRKDGZ0YFsgCdf8qbLCxIQHQX_Ee1JoMVaw7CB_6G0Z12xB5Ws--0TE1pRv_HzWfafz-j0vC17xaJad1HnpmnqV6cX-jj488v44T3jp48-KXUe1L6jap94SB5odPhkemNveLCyr91yATvP2_C9yPD6ACSTG_oP3st28y8wX3owJX7S1OQlpDc3-7hMO1twNMBbDxPW5Ev7GKQAaRj8ZaTB6DzEcQirJRBJFxD4wKS8UyI8jIaJ-FaX-eZpd2D6mMTwD6fYrlg2544ozG1EZ8mx-4wVVFi_DyoDbBV_bZSZ0bTA"
    }
});