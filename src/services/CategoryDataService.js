import http from '@/http-common';

class CategoryDataService {
    get(id) {
        return http.get(`/categories/${id}`);
    }
}

export default new CategoryDataService();