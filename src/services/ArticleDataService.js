import http from '@/http-common';

class ArticleDataService {
    getAll() {
        return http.get('/articles');
    }

    get(id) {
        return http.get(`/articles/${id}`);
    }

    store(data) {
        return http.post('/articles', data);
    }   

    update(id, data) {
        return http.put(`/articles/${id}`, data);
    }

    delete(id) {
        return http.delete(`/articles/${id}`);
    }

    filterByCategory(id) {
        return http.get(`/articles/search?cat_id=${id}`);
    }
}

export default new ArticleDataService();