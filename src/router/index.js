import { createRouter, createWebHashHistory } from 'vue-router';

const routes = [
    {
        path: '/',
        alias: '/articles',
        name: 'articles',
        component: () => import('@/views/ViewArticles.vue')
    },
    {
        path: '/edit-article/:id',
        name: 'edit-article',
        component: () => import('@/views/ViewEditArticle.vue')
    },
    {
        path: '/article/:id',
        name: 'article-detail',
        component: () => import('@/views/ViewShowArticle.vue')
    },
    {
        path: '/category/:id',
        name: 'category-archive',
        component: () => import('@/views/ViewCategoryArchive.vue')
    }
];

const router = createRouter({
    history: createWebHashHistory(),
    routes
});

export default router;