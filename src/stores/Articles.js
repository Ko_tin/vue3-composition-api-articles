import { ref } from "vue"; 
import { defineStore } from "pinia"; 
import ArticleDataService from '@/services/ArticleDataService';

export const useArticlesStore = defineStore('articles', () => {
  /**
   * state properties
   */
  const articles = ref([]);

  /**
   * actions
   */
  function addArticle(newArticle) {
    ArticleDataService.store(newArticle)
      .then(response => {
        articles.value = [];
        this.retrieveArticles();
      })
      .catch(e => console.log(e));
  }

  // Delete from db
  function deleteArticle(idToDelete) {
    ArticleDataService.delete(idToDelete)
      .then(response => {
        articles.value = [];  // clear
        this.retrieveArticles();  // refect articles
      })
      .catch(e => console.log(e));
  }

  function retrieveArticles() {
    ArticleDataService.getAll()
      .then(response => {
        articles.value = response.data.results;
      })
      .catch(e => console.log(e));
  }

  return { 
    // state properties
    articles, 
    // actions
    addArticle, deleteArticle, retrieveArticles
  };
})